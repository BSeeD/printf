/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmontene <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/09 14:29:41 by jmontene          #+#    #+#             */
/*   Updated: 2016/11/09 14:30:12 by jmontene         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_atoi(const char *str)
{
	int		sign;
	int		i;
	int		out;

	sign = 1;
	i = 0;
	out = 0;
	while ((str[i] == ' ') || ((str[i] <= '\r') && (str[i] >= '\t')))
		i++;
	if (str[i] == '-')
	{
		sign = -1;
		i++;
	}
	else if (str[i] == '+')
		i++;
	while ((str[i] <= '9') && (str[i] >= '0'))
	{
		out = (out * 10) + str[i] - '0';
		i++;
	}
	return (out * sign);
}
