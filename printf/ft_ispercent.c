/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ispercent.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmontene <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/11 17:59:31 by jmontene          #+#    #+#             */
/*   Updated: 2017/03/12 17:50:03 by jmontene         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int			is_conversion(char c)
{
	if ((c == 's') || (c == 'S') || (c == 'p') || (c == 'd')
			|| (c == 'D') || (c == 'i') || (c == 'o') || (c == 'O')
			|| (c == 'u') || (c == 'U') || (c == 'x') || (c == 'X')
			|| (c == 'c') || (c == 'C') || (c == '%'))
		return (1);
	return (0);
}

int			is_modifier(char c)
{
	if ((c == 'h') || (c == 'l') || (c == 'j') || (c == 'z'))
		return (1);
	return (0);
}

int			is_flag(char c)
{
	if ((c == '#') || (c == '-') || (c == '+') || (c == ' ')
			|| (c == '0') || (c == '.'))
		return (1);
	return (0);
}

static void	handle_type(t_hd *hd, char c, int signe)
{
	hd->mod = c;
	hd->conv += (signe * 32);
}

void		get_type(t_hd *hd)
{
	int i;

	i = 0;
	if (hd->conv == 'p')
		hd->mod = 'l';
	if ((hd->conv == 'D') || (hd->conv == 'O') || (hd->conv == 'U'))
		handle_type(hd, 'l', 1);
	if (((hd->conv == 'c') || (hd->conv == 's')) && (hd->mod == 'l'))
		handle_type(hd, '0', -1);
	if (!((hd->conv == 'C') || (hd->conv == 'c') ||
				(hd->conv == 's') || (hd->conv == 'S')))
		while (i < TYPES_NB)
		{
			if (hd->mod == g_int_typelist[i].mod)
			{
				if ((hd->conv == 'd' || hd->conv == 'i'))
					hd->type = g_int_typelist[i].type;
				else
					hd->type = g_typelist[i].type;
			}
			i++;
		}
}
