/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_num2.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmontene <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/12 18:20:35 by jmontene          #+#    #+#             */
/*   Updated: 2017/03/12 18:20:36 by jmontene         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		handle_sharp(t_hd *hd)
{
	int count;

	count = 0;
	if ((hd->conv == 'o') &&
		((ft_strcmp("0", hd->out)) || hd->flags.bits.prec))
	{
		write(1, "0", 1);
		count++;
	}
	if (((hd->conv == 'x') &&
		(ft_strcmp("0", hd->out))) || (hd->conv == 'p'))
	{
		write(1, "0x", 2);
		count += 2;
	}
	if (hd->conv == 'X')
	{
		if (ft_strcmp("0", hd->out))
		{
			write(1, "0X", 2);
			count += 2;
		}
	}
	return (count);
}

int		handle_flag(t_hd *hd, char *str)
{
	(void)hd;
	write(1, str, 1);
	return (1);
}
