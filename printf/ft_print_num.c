/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_num.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmontene <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/11 17:52:33 by jmontene          #+#    #+#             */
/*   Updated: 2017/03/12 18:42:02 by jmontene         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static void		ft_prepadding_init(t_hd *hd, int fcount)
{
	if ((hd->out[0] == '-') && (hd->flags.bits.prec))
		hd->pvalue++;
	if ((hd->pvalue < fcount) &&
			((ft_strcmp("0", hd->out)) || (hd->conv == 'p')))
		hd->pvalue = fcount;
	if (hd->pvalue > hd->wvalue)
		hd->wvalue = hd->pvalue;
	if ((hd->flags.bits.plus) || (hd->flags.bits.space))
		hd->wvalue--;
	if (hd->flags.bits.sharp && (hd->conv == 'o'))
		hd->wvalue--;
	if (hd->flags.bits.sharp &&
			((hd->conv == 'x') || (hd->conv == 'p') ||
			(hd->conv == 'X')) &&
			(!(hd->flags.bits.prec || (!hd->flags.bits.width))))
		hd->wvalue -= 2;
}

static int		pre_padding(t_hd *hd, int fcount)
{
	int count;

	count = 0;
	ft_prepadding_init(hd, fcount);
	if ((!hd->flags.bits.minus) && (!hd->flags.bits.zero))
		while (hd->wvalue > hd->pvalue)
			(count += handle_flag(hd, " ")) ? hd->wvalue-- : 0;
	if (hd->flags.bits.plus)
		count += handle_flag(hd, "+");
	if (hd->out[0] == '-')
		count += handle_flag(hd, "-");
	if (hd->flags.bits.space)
		count += handle_flag(hd, " ");
	if (hd->flags.bits.sharp)
		count += handle_sharp(hd);
	if ((!hd->flags.bits.minus && hd->wvalue > fcount) ||
			hd->flags.bits.zero)
		while (hd->wvalue > fcount)
			(count += handle_flag(hd, "0")) ? hd->wvalue-- : 0;
	if (hd->flags.bits.minus && (hd->pvalue > fcount))
		while (hd->pvalue > fcount)
			(count += handle_flag(hd, "0")) ? hd->wvalue-- && hd->pvalue-- : 0;
	if (hd->flags.bits.minus)
		hd->wvalue -= fcount;
	return (count);
}

static int		post_padding(t_hd *hd)
{
	int count;

	count = 0;
	if (hd->flags.bits.minus)
		while (hd->wvalue > 0)
		{
			write(1, " ", 1);
			hd->wvalue--;
			count++;
		}
	return (count);
}

static int		ft_write_num(t_hd *hd)
{
	if ((ft_strcmp("0", hd->out)) || (!hd->flags.bits.prec) ||
			(hd->pvalue > 1))
	{
		if (hd->out[0] == '-')
		{
			write(1, &hd->out[1], ft_strlen(&hd->out[1]));
			return (ft_strlen(&hd->out[1]));
		}
		else
		{
			write(1, hd->out, ft_strlen(hd->out));
			return (ft_strlen(hd->out));
		}
	}
	return (0);
}

int				ft_printf_num(t_hd *hd, va_list *ap)
{
	int i;
	int count;

	i = -1;
	count = 0;
	hd->out = apply_itoa(hd, ap);
	if (hd->out[0] == '-')
	{
		hd->flags.bits.plus = 0;
		hd->flags.bits.space = 0;
	}
	count += pre_padding(hd, ft_strlen(hd->out));
	if ((hd->conv == 'x') || (hd->conv == 'p'))
		while (hd->out[++i])
			if ((hd->out[i] >= 'A') && (hd->out[i] <= 'F'))
				hd->out[i] += 32;
	count += ft_write_num(hd);
	count += post_padding(hd);
	free(hd->out);
	return (count);
}
