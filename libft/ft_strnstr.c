/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmontene <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/07 14:27:07 by jmontene          #+#    #+#             */
/*   Updated: 2016/11/16 16:53:46 by jmontene         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnstr(const char *big, const char *l, size_t len)
{
	size_t	i;
	char	*temp;

	temp = (char *)big;
	if (l[0] == '\0')
		return (temp);
	while ((ft_strlen(temp) >= ft_strlen(l)) && (ft_strlen(l) <= len))
	{
		i = 0;
		while ((temp[i] != '\0') && (temp[i] == l[i]))
			i++;
		if (i == ft_strlen(l))
			return (temp);
		temp++;
		len--;
	}
	return (0);
}
