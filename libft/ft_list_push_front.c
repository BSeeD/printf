/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_push_front.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmontene <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/16 17:59:35 by jmontene          #+#    #+#             */
/*   Updated: 2016/11/16 18:03:25 by jmontene         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void				ft_list_push_front(t_list **begin_list, void *data)
{
	t_list *temp;

	if (begin_list)
	{
		temp = ft_create_elem(data);
		if (temp && *begin_list)
		{
			temp->next = *begin_list;
			*begin_list = temp;
		}
		else
			*begin_list = ft_create_elem(data);
	}
}
