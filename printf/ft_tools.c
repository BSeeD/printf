/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtools.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmontene <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/11 18:05:38 by jmontene          #+#    #+#             */
/*   Updated: 2017/03/12 18:19:16 by jmontene         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		ft_wstrlen(wchar_t *s)
{
	int i;

	i = 0;
	while (*s)
	{
		if (*s <= 0x7F)
			i++;
		else if (*s <= 0x7FF)
			i += 2;
		else if (*s <= 0xFFFF)
			i += 3;
		else if (*s <= 0x10FFFF)
			i += 4;
		s++;
	}
	return (i);
}

int		ft_wcharlen(wchar_t c)
{
	if (c <= 0x7F)
		return (1);
	else if (c <= 0x7FF)
		return (2);
	else if (c <= 0xFFFF)
		return (3);
	else if (c <= 0x10FFFF)
		return (4);
	return (-1);
}

void	deal_modifier(const char *str, t_hd *hd)
{
	int i;

	i = 0;
	while (is_modifier(str[i]))
		i++;
	if (i == 1)
		hd->mod = (unsigned char)str[0];
	else if (str[0] == 'l')
		hd->mod = 'L';
	else
		hd->mod = 'H';
}

int		ft_write_null(void)
{
	write(1, "(null)", 6);
	return (6);
}
