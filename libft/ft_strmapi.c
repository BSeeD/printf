/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmapi.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmontene <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/10 15:01:58 by jmontene          #+#    #+#             */
/*   Updated: 2016/11/10 15:03:34 by jmontene         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

char		*ft_strmapi(char const *s, char (*f)(unsigned int, char))
{
	int		i;
	char	*temp;

	if (s)
	{
		i = ft_strlen(s);
		if (!(temp = (char *)malloc(sizeof(*temp) * (i + 1))))
			return (0);
		temp[i] = 0;
		while (i--)
			temp[i] = f(i, s[i]);
		return (temp);
	}
	return (0);
}
