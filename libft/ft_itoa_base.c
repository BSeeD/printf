/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa_base.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: exam <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/29 10:43:41 by exam              #+#    #+#             */
/*   Updated: 2017/03/12 19:38:04 by jmontene         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

static int		count_figures(unsigned long value, int base)
{
	int i;

	i = 0;
	if (value == 0)
		i++;
	while (value)
	{
		value = value / base;
		i++;
	}
	return (i);
}

static int		ft_init(char *refbase)
{
	int i;

	i = -1;
	while (++i < 16)
		refbase[i] = (i < 10) ? '0' + i : 'A' + (i - 10);
	return (0);
}

char			*ft_itoa_base(long long value, int base, int is_signed)
{
	char	refbase[16];
	char	*out;
	int		signe;
	int		i;

	signe = ft_init(&refbase[0]);
	if (value < 0 && base == 10 && is_signed)
	{
		value = -value;
		signe = 1;
	}
	i = count_figures((unsigned long)value, base) + signe;
	if (!(out = (char*)malloc(sizeof(*out) * (i + 1))))
		return (0);
	out[i] = '\0';
	if (value == 0)
		out[0] = '0';
	while (value ? i-- : 0)
	{
		out[i] = refbase[(unsigned long)value % base];
		value = (unsigned long)value / base;
	}
	if (signe)
		out[0] = '-';
	return (out);
}
