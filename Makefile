# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: jmontene <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/02/22 14:28:24 by jmontene          #+#    #+#              #
#    Updated: 2017/03/12 18:20:20 by jmontene         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

CC = gcc
CFLAGS =-Wall -Wextra -Werror
NAME = libftprintf.a
LIBDIR = ./libft
PRINTFDIR = ./printf
LIB_SRC = ft_atoi.c ft_bzero.c ft_isalnum.c ft_isalpha.c ft_isascii.c ft_isdigit.c \
	  ft_isprint.c ft_itoa.c ft_create_elem.c ft_list_last.c \
	  ft_lstadd.c ft_lstdel.c ft_lstdelone.c ft_lstiter.c ft_lstmap.c \
	  ft_lstnew.c ft_memalloc.c ft_memccpy.c ft_memchr.c ft_memcmp.c \
	  ft_memcpy.c ft_memdel.c ft_memmove.c ft_memset.c \
	  ft_putchar.c ft_putchar_fd.c ft_putendl.c ft_putendl_fd.c ft_putnbr.c \
	  ft_putnbr_fd.c ft_putstr.c ft_putstr_fd.c ft_strcat.c \
	  ft_strchr.c ft_strclr.c ft_strcmp.c ft_strcpy.c ft_strdel.c ft_strdup.c \
	  ft_strequ.c ft_striter.c ft_striteri.c ft_strjoin.c ft_strlcat.c \
	  ft_strlen.c ft_strmap.c ft_strmapi.c ft_strncat.c ft_strncmp.c \
	  ft_strncpy.c ft_strnequ.c ft_strnew.c ft_strnstr.c ft_strrchr.c \
	  ft_strsplit.c ft_strstr.c ft_strsub.c ft_strtrim.c ft_tolower.c \
	  ft_toupper.c ft_list_push_back.c ft_list_push_front.c ft_list_size.c \
	  ft_itoa_base.c ft_putwchar.c get_next_line.c
LIB_OBJ = $(LIB_SRC:.c=.o)
PRINTF_SRC = ft_flagrules.c ft_flags.c ft_ispercent.c ft_print_char.c ft_print_num.c ft_print_num2.c \
		ft_print_percent.c ft_print_str.c ft_print_str2.c ft_printf.c ft_tools.c ft_types.c
PRINTF_OBJ = $(PRINTF_SRC:.c=.o)
OBJ = $(LIB_OBJ) $(PRINTF_OBJ)

.PHONY: clean fclean re

all: $(NAME)

exe: $(NAME)
	$(CC) $(CFLAGS) main.c $(NAME) -I $(LIBDIR)/includes -I $(PRINTFDIR)

$(NAME): $(OBJ)
	@ar rc $(NAME) $(OBJ)
	@ranlib $(NAME)
	@echo "made" $(NAME)

$(LIB_OBJ): %.o: $(LIBDIR)/%.c
	@$(CC) -c $(CFLAGS) -I $(LIBDIR)/includes $< -o $@

$(PRINTF_OBJ): %.o: $(PRINTFDIR)/%.c
	@$(CC) -c $(CFLAGS) -I $(LIBDIR)/includes -I $(PRINTFDIR) $< -o $@

clean:
	@rm -rf $(OBJ)
	@echo "cleaned" $(NAME)

fclean: clean
	@rm -rf $(NAME)
	@echo "fcleaned" $(NAME)

re: fclean all
