/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_char.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmontene <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/11 17:52:50 by jmontene          #+#    #+#             */
/*   Updated: 2017/03/11 19:16:06 by jmontene         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		ft_printf_char(t_hd *hd, va_list *ap)
{
	unsigned char		c;
	int					count;

	count = 0;
	c = (unsigned char)va_arg(*ap, int);
	if ((!hd->flags.bits.minus) && (hd->flags.bits.width))
		while (hd->wvalue > 1)
		{
			write(1, " ", 1);
			hd->wvalue--;
			count++;
		}
	write(1, &c, 1);
	if ((hd->flags.bits.minus) && (hd->flags.bits.width))
		while (hd->wvalue > 1)
		{
			write(1, " ", 1);
			hd->wvalue--;
			count++;
		}
	return (count + 1);
}

int		ft_printf_wchar(t_hd *hd, va_list *ap)
{
	wchar_t	c;
	int		i;
	int		count;

	i = 0;
	count = 0;
	c = (wchar_t)va_arg(*ap, wchar_t);
	if ((!hd->flags.bits.minus) && (hd->flags.bits.width))
		while (hd->wvalue > 1)
		{
			write(1, " ", 1);
			hd->wvalue--;
			count++;
		}
	if (!(i = ft_putwchar(c)))
		return (-1);
	count += i;
	if ((hd->flags.bits.minus) && (hd->flags.bits.width))
		while (hd->wvalue > 1)
		{
			write(1, " ", 1);
			hd->wvalue--;
			count++;
		}
	return (count);
}
