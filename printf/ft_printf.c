/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmontene <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/12 18:07:25 by jmontene          #+#    #+#             */
/*   Updated: 2017/03/13 12:09:09 by jmontene         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static int	ft_ctf(const char *str, t_hd *hd, int i, int fct)
{
	if (fct == 1)
	{
		hd->pvalue = ft_atoi(&str[++i]);
		while (ft_isdigit(str[i]))
			i++;
		i--;
	}
	else if (fct == 2)
	{
		hd->flags.bits.width = 1;
		hd->wvalue = ft_atoi(&str[i]);
		while (ft_isdigit(str[i]))
			i++;
		i--;
	}
	return (i);
}

static int	ft_capture_the_flags(const char *str, t_hd *hd)
{
	int i;

	i = 1;
	while (!is_conversion(str[i]) && str[i])
	{
		if (is_modifier(str[i]) && (!hd->flags.bits.mod))
			deal_modifier(&str[i], hd);
		if ((is_flag(str[i])) || (is_modifier(str[i])))
			set_flags(hd, str[i]);
		if (str[i] == '.')
			i = ft_ctf(str, hd, i, 1);
		else if (!is_flag(str[i]) && ft_isdigit(str[i]))
			i = ft_ctf(str, hd, i, 2);
		i++;
	}
	if (!hd->flags.bits.mod)
		hd->mod = '0';
	hd->conv = (unsigned char)str[i];
	get_type(hd);
	unset_flags(hd);
	return (i + 1);
}

static int	ft_treat_the_flags(const char *str, t_hd *hd, va_list *ap)
{
	int i;
	int count;

	i = 0;
	count = 0;
	(void)str;
	while (i < FUNC_NB)
	{
		if (hd->conv == g_printf_func[i].id)
			count += g_printf_func[i].f(hd, ap);
		i++;
	}
	return (count);
}

static void	ft_handle_init(t_hd *hd)
{
	hd->flags.value = 0;
	hd->pvalue = 0;
	hd->wvalue = 0;
	hd->out = NULL;
}

int			ft_printf(const char *format, ...)
{
	va_list	ap;
	t_hd	hd;
	int		i;
	int		j;
	int		count;

	i = 0;
	count = 0;
	va_start(ap, format);
	while (format[i])
	{
		ft_handle_init(&hd);
		j = i;
		while ((format[i] != '%') && (format[i]))
			i++;
		write(1, &format[j], i - j);
		count += i - j;
		if (!format[i])
			return (count);
		j = i + ft_capture_the_flags(&format[i], &hd);
		count += ft_treat_the_flags(&format[i], &hd, &ap);
		i = j;
	}
	va_end(ap);
	return (count);
}
