/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmontene <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/07 14:16:36 by jmontene          #+#    #+#             */
/*   Updated: 2016/11/08 16:53:00 by jmontene         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memchr(const void *s, int c, size_t n)
{
	int				i;
	unsigned char	*temps;

	i = 0;
	temps = (unsigned char *)s;
	while (n)
	{
		if (temps[i] == (unsigned char)c)
			return (&temps[i]);
		i++;
		n--;
	}
	return (0);
}
