/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_types.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmontene <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/10 13:16:57 by jmontene          #+#    #+#             */
/*   Updated: 2017/03/11 19:50:32 by jmontene         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static int			get_base(t_hd *hd)
{
	if (hd->conv == 'X' || hd->conv == 'x' || hd->conv == 'p')
		return (16);
	else if (hd->conv == 'o')
		return (8);
	else if (hd->conv == 'b')
		return (2);
	else
		return (10);
}

static char			*apply_itoa_int(t_hd *hd, va_list *ap)
{
	char *s;

	if (!ft_strcmp(hd->type, "int"))
		s = (ft_itoa_base((int)va_arg(*ap, int), get_base(hd), 1));
	if (!ft_strcmp(hd->type, "short int"))
		s = (ft_itoa_base((short int)va_arg(*ap, int), get_base(hd), 1));
	if (!ft_strcmp(hd->type, "signed char"))
		s = (ft_itoa_base((signed char)va_arg(*ap, int), get_base(hd), 1));
	if (!ft_strcmp(hd->type, "long"))
		s = (ft_itoa_base((long)va_arg(*ap, long), get_base(hd), 1));
	if (!ft_strcmp(hd->type, "long long"))
		s = (ft_itoa_base((long long)va_arg(*ap, long long), get_base(hd), 1));
	if (!ft_strcmp(hd->type, "intmax_t"))
		s = (ft_itoa_base((intmax_t)va_arg(*ap, intmax_t), get_base(hd), 1));
	if (!ft_strcmp(hd->type, "ssize_t"))
		s = (ft_itoa_base((ssize_t)va_arg(*ap, ssize_t), get_base(hd), 1));
	return (s);
}

static char			*apply_itoa_nint2(t_hd *hd, va_list *ap)
{
	char *s;

	if (!ft_strcmp(hd->type, "unsigned long long"))
	{
		s = (ft_itoa_base((unsigned long long)va_arg(*ap, unsigned long long),
					get_base(hd), 0));
	}
	if (!ft_strcmp(hd->type, "uintmax_t"))
		s = (ft_itoa_base((uintmax_t)va_arg(*ap, uintmax_t), get_base(hd), 0));
	if (!ft_strcmp(hd->type, "size_t"))
		s = (ft_itoa_base((size_t)va_arg(*ap, size_t), get_base(hd), 0));
	return (s);
}

static char			*apply_itoa_nint(t_hd *hd, va_list *ap)
{
	char *s;

	if (!ft_strcmp(hd->type, "unsigned int"))
	{
		s = (ft_itoa_base((unsigned int)va_arg(*ap, unsigned int),
			get_base(hd), 0));
	}
	else if (!ft_strcmp(hd->type, "unsigned short"))
	{
		s = (ft_itoa_base((unsigned short)va_arg(*ap, unsigned int),
			get_base(hd), 0));
	}
	else if (!ft_strcmp(hd->type, "unsigned char"))
	{
		s = (ft_itoa_base((unsigned char)va_arg(*ap, unsigned int),
			get_base(hd), 0));
	}
	else if (!ft_strcmp(hd->type, "unsigned long"))
	{
		s = (ft_itoa_base((unsigned long)va_arg(*ap, unsigned long),
			get_base(hd), 0));
	}
	else
		s = apply_itoa_nint2(hd, ap);
	return (s);
}

char				*apply_itoa(t_hd *hd, va_list *ap)
{
	if ((hd->conv == 'd' || hd->conv == 'i'))
		return (apply_itoa_int(hd, ap));
	else
		return (apply_itoa_nint(hd, ap));
}
