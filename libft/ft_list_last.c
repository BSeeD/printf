/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_last.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmontene <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/16 18:01:31 by jmontene          #+#    #+#             */
/*   Updated: 2016/11/16 18:01:59 by jmontene         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list		*ft_list_last(t_list *begin_list)
{
	t_list *temp;

	temp = begin_list;
	if (temp)
	{
		while (temp->next)
			temp = temp->next;
	}
	return (temp);
}
