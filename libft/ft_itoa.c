/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmontene <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/11 15:02:45 by jmontene          #+#    #+#             */
/*   Updated: 2016/11/16 16:58:37 by jmontene         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

static int		ft_count(int nb)
{
	int	i;

	i = 0;
	while (nb)
	{
		nb = nb / 10;
		i++;
	}
	return (i);
}

static char		*ft_fill(char *temp, int nb, int i)
{
	while (nb)
	{
		i--;
		temp[i] = nb % 10 + '0';
		nb = nb / 10;
	}
	return (temp);
}

char			*ft_itoa(int n)
{
	int		signe;
	int		count;
	int		i;
	char	*temp;

	signe = 0;
	count = 1;
	if (n < 0)
	{
		signe = 1;
		n = -n;
	}
	i = ft_count(n) + signe;
	if (!n)
		i = 1;
	if (!(temp = (char *)malloc(sizeof(*temp) * (i + 1))))
		return (0);
	if (signe)
		temp[0] = '-';
	if (!n)
		temp[0] = '0';
	temp[i] = '\0';
	temp = ft_fill(temp, n, i);
	return (ft_strcmp(temp, "-./,),(-*,(") ?
			temp : ft_strcpy(temp, "-2147483648"));
}
